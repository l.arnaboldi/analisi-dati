import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import tqdm
from sklearn import datasets
from sklearn.utils import shuffle

# Matplotlib style
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx} \\usepackage{amsfonts}')
plt.style.use('seaborn')

figsize = (7,6)

# Disable annoying future warnings
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Compact progress bar
def pbar(*args, **kwargs):
  return tqdm.tqdm(*args, **kwargs, leave = False, ncols = 70)


## sklearn
random_state = 0

def shuffle_dataset(dataset):
  def shuffled_dataset():
    return shuffle(*dataset(), random_state=random_state)
  
  return shuffled_dataset

# Boston dataset
@shuffle_dataset
def boston_dataset():
  boston_ds = datasets.load_boston()
  # Y = MEDV(Median value of owner-occupied homes in $1000's)
  return boston_ds.data, boston_ds.target

# Iris dataset
@shuffle_dataset
def iris_dataset():
  iris_ds = datasets.load_iris()
  # Y = species
  return iris_ds.data, iris_ds.target

# MNIST
@shuffle_dataset
def mnist_dataset():
  return datasets.fetch_openml('mnist_784', cache=True, return_X_y=True, as_frame=False)

def binary_mnist_dataset(first_digit=0, second_digit=1):
  X, Y = mnist_dataset()
  to_keep = np.bitwise_or((Y == str(first_digit)), (Y == str(second_digit)))
  return X[to_keep], Y[to_keep]

# Titanic
def titanic_dataset():
  return datasets.fetch_openml('titanic', cache = True, version=1, as_frame=True)

# Reduce the features given a mask
def reduce_feature_given_mask(X, selected):
  return (X.transpose()[list(selected)]).transpose()
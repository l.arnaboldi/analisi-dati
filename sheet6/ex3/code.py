from numpy import vectorize
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_classification

from analisi_dati import *

from matplotlib import cm #Color Map

M_range = np.arange(1,20, dtype=np.uint32)
J_range = np.arange(1,20, dtype=np.uint32)

def compute_dataset(name, X, Y):
  X_train, X_test, Y_train, Y_test = train_test_split(
    X, Y,
    test_size=min(1000, int(X.shape[0]/10)),
    random_state=random_state
  )

  @np.vectorize
  def errors_fixed_JM(J, M):
    print(J,M)
    ada = AdaBoostClassifier(
      base_estimator=DecisionTreeClassifier(max_depth=J),
      n_estimators=M,
      random_state=random_state
    )
    ada.fit(X_train, Y_train)
    return (
      ada.score(X_train, Y_train),
      ada.score(X_test, Y_test)
    )


  J = np.array(J_range)
  M = np.array(M_range)
  J, M = np.meshgrid(J, M)
  train_error, test_error = errors_fixed_JM(J, M)

  fig_train = plt.figure(figsize=figsize,)
  ax_train = fig_train.add_subplot(projection='3d')
  ax_train.plot_surface(J, M,  100*np.array(train_error), cmap=cm.coolwarm, linewidth=0, antialiased=False)
  ax_train.set_xlim(1,max(J_range))
  ax_train.set_ylim(1,max(M_range))
  ax_train.set_xlabel('$J$')
  ax_train.set_ylabel('$M$')
  ax_train.set_zlabel('Accuracy $\\left[\\si{\\percent}\\right]$')
  ax_train.view_init(elev=20, azim=240)
  fig_train.savefig(f'accuracies-{name}-train.pdf', format = 'pdf', bbox_inches = 'tight')

  fig_test = plt.figure(figsize=figsize,)
  ax_test = fig_test.add_subplot(projection='3d')
  ax_test.plot_surface(J, M,  100*np.array(test_error), cmap=cm.coolwarm, linewidth=0, antialiased=False)
  ax_test.set_xlim(1,max(J_range))
  ax_test.set_ylim(1,max(M_range))
  ax_test.set_xlabel('$J$')
  ax_test.set_ylabel('$M$')
  ax_test.set_zlabel('Accuracy $\\left[\\si{\\percent}\\right]$')
  ax_test.view_init(elev=20, azim=240)
  fig_test.savefig(f'accuracies-{name}-test.pdf', format = 'pdf', bbox_inches = 'tight')

X_mn, Y_mn = mnist_dataset()

X_ah, Y_ah = make_classification(
  n_samples=1000,
  n_features=100,
  n_informative=40,
  n_redundant=40,
  n_classes=10,
  class_sep = 5.,
  random_state=random_state
)

compute_dataset('mnist', X_mn, Y_mn)
compute_dataset('adhoc', X_ah, Y_ah)
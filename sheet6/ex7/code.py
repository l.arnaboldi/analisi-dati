from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_classification

from analisi_dati import *

M_range = np.arange(1,20, dtype=np.uint32)

def compute_dataset(name, X, Y):
  X_train, X_test, Y_train, Y_test = train_test_split(
    X, Y,
    test_size=1000,
    random_state=random_state
  )

  def errors_fixed_M(M):
    rf = RandomForestClassifier(
      n_estimators=M,
      max_depth=100,
      min_samples_split=100,
      random_state=random_state
    )
    rf.fit(X_train, Y_train)
    return (
      rf.score(X_train, Y_train),
      rf.score(X_test, Y_test)
    )

  train_error, test_error = zip(*[errors_fixed_M(m) for m in pbar(M_range)])

  fig, ax = plt.subplots(figsize=figsize)
  ax.plot(M_range, 100*np.array(train_error), label='Train Error')
  ax.plot(M_range, 100*np.array(test_error), label='Test Error')
  ax.legend()
  ax.set_xlim(1,max(M_range))
  ax.set_xlabel('$M$')
  ax.set_ylabel('Accuracy $\\left[\\si{\\percent}\\right]$')
  fig.savefig(f'accuracies-{name}.pdf', format = 'pdf', bbox_inches = 'tight')

X_mn, Y_mn = mnist_dataset()

X_ah, Y_ah = make_classification(
  n_samples=X_mn.shape[0],
  n_features=X_mn.shape[1],
  n_informative=int(0.4*X_mn.shape[1]),
  n_redundant=int(0.4*X_mn.shape[1]),
  n_classes=10,
  class_sep = 10.,
  random_state=random_state
)

compute_dataset('mnist', X_mn, Y_mn)
compute_dataset('adhoc', X_ah, Y_ah)


import os
from numpy.matrixlib.defmatrix import _convert_from_string
from sklearn.decomposition import PCA
from PIL import Image

from analisi_dati import *

ds_path = 'yale-face-centered/'
output_path = 'components-centered/'
n_components = 165

dataset = np.array([plt.imread(ds_path+fn) for fn in os.listdir(ds_path)])
images_shape = dataset.shape[1:]
dataset = dataset.reshape(dataset.shape[0], -1)

pca = PCA(n_components=n_components)
pca.fit(dataset)
components = pca.components_.reshape(n_components, *images_shape)

positive_components = components - np.amin(components, axis=(1,2), keepdims=True)
components_normalized = positive_components / np.amax(positive_components, axis=(1,2), keepdims=True)
components256 = np.uint8(255*components_normalized)


for c, im in enumerate(components256):
  # if im[-1][0] > 128:
  #   im = 255 - im
  image = Image.fromarray(im, 'L')
  image.save(output_path + f'{c}.png')
plt.show()


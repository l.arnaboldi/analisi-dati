
from sklearn.linear_model import Lasso, Ridge
from sklearn.model_selection import cross_validate
from sklearn.metrics import mean_squared_error

from analisi_dati import *

scoring = 'neg_mean_squared_error'

X, Y = boston_dataset()


# alpha = lambda (we can't use lambda keyword in Python)
def regression(Model, alpha):
  model = Model(alpha=alpha)
  model.fit(X, Y)
  
  cv_result = cross_validate(Model(alpha=alpha), X, Y, scoring=scoring)

  return (
    model.coef_, # Coefficients
    mean_squared_error(model.predict(X), Y), # Train Error
    np.mean(cv_result[f'test_score']) # Test Error
  )

alphas_lasso = np.logspace(-3.2, 3.2, num=1000)
alphas_ridge = np.logspace(-1, 10.2, num=1000)
lasso_coef, lasso_train, neg_lasso_test = map(np.array,zip(*[regression(Lasso, a) for a in pbar(alphas_lasso)]))
ridge_coef, ridge_train, neg_ridge_test = map(np.array,zip(*[regression(Ridge, a) for a in pbar(alphas_ridge)]))

fig_err_lasso, ax_err_lasso = plt.subplots(figsize=figsize)
ax_err_lasso.set_xlabel('$\\lambda$')
ax_err_lasso.set_ylabel('MSE')
ax_err_lasso.set_xscale('log')
ax_err_lasso.plot(alphas_lasso, lasso_train, label='Train Error Lasso')
ax_err_lasso.plot(alphas_lasso, -neg_lasso_test, label='Test Error Lasso')
ax_err_lasso.legend()
fig_err_lasso.savefig('errors-lasso.pdf', format = 'pdf', bbox_inches = 'tight')

fig_coeff_lasso, ax_coeff_lasso = plt.subplots(figsize=figsize)
ax_coeff_lasso.set_xlabel('$\\lambda$')
ax_coeff_lasso.set_ylabel('Coefficients')
ax_coeff_lasso.set_xscale('log')
ax_coeff_lasso.set_ylim([-2.5,4])
ax_coeff_lasso.plot(alphas_lasso, lasso_coef)
fig_coeff_lasso.savefig('coeff-lasso.pdf', format = 'pdf', bbox_inches = 'tight')

fig_err_ridge, ax_err_ridge = plt.subplots(figsize=figsize)
ax_err_ridge.set_xlabel('$\\lambda$')
ax_err_ridge.set_ylabel('MSE')
ax_err_ridge.set_xscale('log')
ax_err_ridge.plot(alphas_ridge, ridge_train, label='Train Error Ridge')
ax_err_ridge.plot(alphas_ridge, -neg_ridge_test, label='Test Error Ridge')
ax_err_ridge.legend()
fig_err_ridge.savefig('errors-ridge.pdf', format = 'pdf', bbox_inches = 'tight')

fig_coeff_ridge, ax_coeff_ridge = plt.subplots(figsize=figsize)
ax_coeff_ridge.set_xlabel('$\\lambda$')
ax_coeff_ridge.set_ylabel('Coefficients')
ax_coeff_ridge.set_xscale('log')
ax_coeff_ridge.set_ylim([-2.5,4])
ax_coeff_ridge.plot(alphas_ridge, ridge_coef)
fig_coeff_ridge.savefig('coeff-ridge.pdf', format = 'pdf', bbox_inches = 'tight')
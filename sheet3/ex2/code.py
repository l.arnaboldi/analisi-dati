from matplotlib.pyplot import legend
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

from itertools import product

from analisi_dati import *

fig_mse, ax_mse = plt.subplots(figsize=figsize)
fig_r2, ax_r2 = plt.subplots(figsize=figsize)

X, Y = boston_dataset()

n_feat = X.shape[1]

def mse_r2_from_lr(X, Y):
  lr = LinearRegression()
  lr.fit(X, Y)
  return mean_squared_error(lr.predict(X), Y), lr.score(X, Y)

# All subsets
for selected in pbar(product([False, True], repeat=n_feat), total=2**n_feat):
  features = selected.count(True)
  if features == 0:
    continue
  reducedX = reduce_feature_given_mask(X, selected)
  mse, r2 = mse_r2_from_lr(reducedX, Y)
  ax_mse.plot(features, mse, marker='.', c='r', alpha=.5)
  ax_r2.plot(features, r2, marker='.', c='r', alpha=.5)

# Forward & Backward
from sklearn.feature_selection import SequentialFeatureSelector

def selected_features(n, direction):
  lr = LinearRegression()
  sfs = SequentialFeatureSelector(lr, n_features_to_select=n, direction=direction)
  sfs.fit(X,Y)
  return list(sfs.get_support())

fs = [selected_features(n, direction='forward') for n in pbar(range(1,n_feat))]
bs = [selected_features(n, direction='backward') for n in pbar(range(1,n_feat))]

f_result = [mse_r2_from_lr(reduce_feature_given_mask(X, s), Y) for s in pbar(fs)]
b_result = [mse_r2_from_lr(reduce_feature_given_mask(X, s), Y) for s in pbar(bs)]

f_result.append(mse_r2_from_lr(X,Y))
b_result.append(mse_r2_from_lr(X,Y))

f_mse, f_r2 = zip(*f_result)
b_mse, b_r2 = zip(*b_result)

feat_array = np.arange(1,n_feat+1)

ax_mse.plot(feat_array, f_mse, label='Forward')
ax_r2.plot(feat_array, f_r2, label='Forward')
ax_mse.plot(feat_array, b_mse, label='Backward')
ax_r2.plot(feat_array, b_r2, label='Backward')

# Typesetting figures
ax_mse.legend()
ax_r2.legend()
ax_mse.set_xlabel('Number of features')
ax_r2.set_xlabel('Number of features')
ax_mse.set_ylabel('MSE')
ax_r2.set_ylabel('$R^2$')
fig_mse.savefig('mse.pdf', format = 'pdf', bbox_inches = 'tight')
fig_r2.savefig('r2.pdf', format = 'pdf', bbox_inches = 'tight')
from tslearn.neighbors import KNeighborsTimeSeriesClassifier
from metrics import *

eknn = KNeighborsTimeSeriesClassifier(
  n_neighbors=5,
  metric='euclidean',
  n_jobs=-1,
)

eknn.fit(ts_eX_train, ecY_train)
epY = eknn.predict(ts_X_valid)
emetrics = evaluate_model(cY_valid, epY)
print(emetrics)
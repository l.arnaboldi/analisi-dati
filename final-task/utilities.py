import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from random import shuffle
from tqdm import tqdm
from sklearn.metrics import confusion_matrix
import seaborn as sns
from matplotlib.colors import PowerNorm

RANDOM = 2203

class plot_style():
  def __init__(self, fontsize=12, latex = True, style='seaborn'):
    # Some check on MatplotLib import
    self.fontsize = fontsize
    self.latex = latex
    self.style = style
  def __enter__(self):
    if self.latex:
      plt.rc('text', usetex=True)
      plt.rc(
        'text.latex',
        preamble=r'\usepackage{amsmath} \usepackage{siunitx} \usepackage{amsfonts}'
      )
    plt.rc('font', size=self.fontsize)
    plt.style.use(self.style)
  
  def __exit__(self, *args):
    plt.rcdefaults()
    plt.style.use('default')
from utilities import *

from matplotlib.cm import get_cmap

## Load dataset
df = pd.read_csv('train.csv', sep=',')
df = df.sample(frac=1, random_state=RANDOM).reset_index(drop=True)

X = df[[f'V{i}' for i in range(2,48)]].values
Y = df['V1'].values
classes = np.unique(Y)
n_class = len(classes)

## Class plots
cmap = get_cmap('hsv')
color_permutation = list(range(max(classes)))
shuffle(color_permutation)
def label_to_color(label, trasparency = 0.05):
  full_color = cmap(((color_permutation[label-1])%n_class)/float(n_class))
  return *full_color[:3], trasparency

def class_plot(X, Y, y_lim = False, name = ''):
  def init_fig():
    fig, ax = plt.subplots(figsize=(6,6))
    if y_lim:
      ax.set_ylim(0., 1.)
    ax.set_xlim(0, X.shape[1])
    ax.set_yticks([])
    ax.set_xticks([])

    return fig, ax

  with plot_style(style='default'):
    t = range(X.shape[1])
    plot_counter = {y:0 for y in classes}
    class_figure = {y:init_fig() for y in classes}

    for i in tqdm(range(len(X))):
      if plot_counter[Y[i]] < 500:
        (class_figure[Y[i]][1]).plot(t, X[i], color=label_to_color(Y[i]))
        plot_counter[Y[i]] += 1

    for y in classes:
      class_figure[y][1].set_title(f'${y}$', fontsize=60)
      (class_figure[y][0]).savefig(f'figures/class-plots/{y}{name}.pdf', format = 'pdf', bbox_inches = 'tight')
      plt.close(class_figure[y][0])



if __name__ == "__main__":
  print(df.describe())
  print(df['V1'].value_counts())

  with plot_style(fontsize=50):
    fig, ax = plt.subplots(figsize=(8,8))
    df.plot.hist(column=['V1'], bins=20, figsize=(7, 6), ax=ax)
    ax.set_xlim(0.2,20.)
    ax.set_xticks(ticks=np.arange(0.7,20.,1.05), labels=list(range(1,20)))
    fig.savefig(f'figures/frequencies-histogram.pdf', format = 'pdf', bbox_inches = 'tight')
    
  class_plot(X,Y,True)
from utilities import *
from preprocessing import *

from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
from sklearn.metrics import make_scorer

# Confusion Matrix
def plot_confusion_matrix(Y_valid, Y_pred, filename = None, show=False):
  # Both Y_valid and Y_pred have to be compressed!
  cf = confusion_matrix(Y_valid, Y_pred)
  cf_true_normalized = cf / cf.astype(float).sum(axis=1,keepdims=True) * 100.
  with plot_style():
    fig, ax = plt.subplots(figsize=(9,9))
    sns.heatmap(cf_true_normalized, annot=True, norm=PowerNorm(0.4),ax=ax,fmt=".1f")
    labels = [str(class_compressed2orginal[cmp]) for cmp in range(n_class)]
    ax.set_xticks(np.arange(n_class)+0.5, labels, rotation=45)
    ax.set_yticks(np.arange(n_class)+0.5, labels, rotation='horizontal')
    ax.set_xlabel(r'Predicted class')
    ax.set_ylabel(r'True class $[\si{\percent}]$')
    if filename is not None:
      fig.savefig(filename, format = 'pdf', bbox_inches = 'tight')
    if show:
      plt.show()

def evaluate_model(Y_valid, Y_pred):
  return (
    accuracy_score(Y_valid, Y_pred),
    recall_score(Y_valid, Y_pred,average='macro'),
    precision_score(Y_valid, Y_pred,average='macro'),
    f1_score(Y_valid, Y_pred,average='macro')
  )

def score_model(Y_valid, Y_pred):
  return accuracy_score(Y_valid, Y_pred) + f1_score(Y_valid, Y_pred,average='macro')
CVscoring = make_scorer(score_model)

def tf_score_model(tfY_valid, tfY_pred):
  Y_valid = np.argmax(tfY_valid, axis=1)
  Y_pred = np.argmax(tfY_pred, axis=1)
  return score_model(Y_valid, Y_pred)

if __name__ == '__main__':
  cY_random = np.minimum(np.random.poisson(7.,X_valid.shape[0]),n_class-1)
  plot_confusion_matrix(cY_valid, cY_random, 'figures/random_confusion_matrix.pdf')
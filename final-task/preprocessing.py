from utilities import *
from loading import *

dataset_path = 'stored-datasets/'

def save_dataset(X, Y,filename):
  with open(dataset_path+filename, 'wb') as f:
    np.save(f, X)
    np.save(f, Y)

def load_dataset(filename):
  with open(dataset_path+filename, 'rb') as f:
    X = np.load(f)
    Y = np.load(f)
  return X, Y

## Y class compression
class_compressed2orginal = dict(enumerate(classes))
class_original2compressed = {o:c for c,o in class_compressed2orginal.items()}
@np.vectorize
def compress_class(y):
  return class_original2compressed[y]
@np.vectorize
def decompress_class(yc):
  return class_compressed2orginal[yc]

cY = compress_class(Y)

## Hold Set Out
from sklearn.model_selection import train_test_split
X_train, X_valid, cY_train, cY_valid = train_test_split(
  X, cY,
  train_size=0.85,
  random_state=RANDOM,
  stratify=cY  # This is needed to mantain the frequencies 
)

## Dataset oversampling
try:
  eX_train, ecY_train = load_dataset('oversampled.npy')
except FileNotFoundError:
  print('Computing oversampling...')
  from imblearn.over_sampling import SMOTE
  oversampler = SMOTE(random_state=RANDOM)
  eX_train, ecY_train = oversampler.fit_resample(X_train,cY_train)
  save_dataset(eX_train, ecY_train,'oversampled.npy')

## Dataset fulloversampling
try:
  eX, ecY = load_dataset('full-oversampled.npy')
except FileNotFoundError:
  print('Computing full oversampling...')
  from imblearn.over_sampling import SMOTE
  oversampler = SMOTE(random_state=RANDOM)
  eX, ecY = oversampler.fit_resample(X,cY)
  save_dataset(eX, ecY,'full-oversampled.npy')

## Dataset Undersampling
try:
  uX_train, ucY_train = load_dataset('undersampled.npy')
except FileNotFoundError:
  print('Computing undersampling...')
  from imblearn.under_sampling import ClusterCentroids
  undersampler = ClusterCentroids(random_state=RANDOM)
  uX_train, ucY_train = undersampler.fit_resample(X_train,cY_train)
  save_dataset(uX_train, ucY_train,'undersampled.npy')
  print('Train Set shape: ', uX_train.shape)

## Dataset Under/Over balancing
class_sizes = [30,1000,2000,5000]
cX_train = {}
ccY_train = {}
for class_size in class_sizes:
  try:
    cX_train[class_size], ccY_train[class_size] = load_dataset(f'overunder{class_size}.npy')
  except FileNotFoundError:
    print(f'Computing combined sampling of size {class_size}...')
    from imblearn.under_sampling import ClusterCentroids, RandomUnderSampler
    st = {c:class_size for c in range(n_class)}
    combinesampler = ClusterCentroids(sampling_strategy=st) #if class_size < 100 else RandomUnderSampler(sampling_strategy=st)
    cX_train[class_size], ccY_train[class_size] = combinesampler.fit_resample(eX_train,ecY_train)
    save_dataset(cX_train[class_size], ccY_train[class_size],f'overunder{class_size}.npy')
    print('Train Set shape: ', cX_train[class_size].shape)


# Fourier Transform of X
eX_train_ft = abs(np.fft.rfft(eX_train))
X_valid_ft =  abs(np.fft.rfft(X_valid))
cX_train_ft = {cs:abs(np.fft.rfft(x)) for cs,x in cX_train.items()}
# Discrete Derivative of X
eX_train_diff = np.diff(eX_train)
X_valid_diff = np.diff(X_valid)

# Time series versions for tslearn
from tslearn.utils import to_time_series_dataset

ts_X_train = to_time_series_dataset(X_train)
ts_X_valid = to_time_series_dataset(X_valid)
ts_eX_train = to_time_series_dataset(eX_train)

# Categorical Output for Neural Networks
from tensorflow.keras.utils import to_categorical
cY_train_cat = to_categorical(cY_train, len(classes))
cY_valid_cat = to_categorical(cY_valid, len(classes))
ecY_train_cat = to_categorical(ecY_train, len(classes))
from utilities import *
from preprocessing import *
from metrics import plot_confusion_matrix, evaluate_model

## Since the random state is the same for every split, the resulting 
# Y_train and Y_valid would be the same, so it's not a problem redifining them

## Test with NN
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import Sequential
from tensorflow.keras.layers import  Input, Dense
import tensorflow.keras as keras

METRICS = [
      keras.metrics.CategoricalAccuracy(name='accuracy'),
      keras.metrics.Precision(name='precision'),
      keras.metrics.Recall(name='recall'),
]

def nn_test(X_train, X_valid,Y_train,Y_valid):
  Y_train_categorical = to_categorical(Y_train, max(classes)+1)
  Y_valid_categorical = to_categorical(Y_valid, max(classes)+1)
  model = Sequential(
      [
          Input(shape=X_train.shape[1]),
          Dense(50, activation='sigmoid'),
          Dense(50, activation='sigmoid'),
          Dense(Y_train_categorical.shape[1], activation='softmax'),
      ]
  )
  model.compile(loss='categorical_crossentropy', optimizer='SGD', metrics=METRICS)
  model.fit(X_train, Y_train_categorical, epochs=10, validation_data=(X_valid, Y_valid_categorical))
  model.evaluate(X_valid, Y_valid_categorical)
  Y_pred = np.argmax(model.predict(X_valid), axis = 1)
  print(evaluate_model(Y_valid, Y_pred))
  plot_confusion_matrix(Y_valid, Y_pred, 'extended_dataset.pdf')
  


# nn_test(eX_train_ft, X_valid_ft,ecY_train, cY_valid)
nn_test(eX_train, X_valid,ecY_train, cY_valid)








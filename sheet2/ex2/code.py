
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

from analisi_dati import *

X, Y = boston_dataset()

def linear_regression_with_split(split):
  assert(0.<split and split<1.)
  X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size=split, random_state=41)
  lr = LinearRegression()
  lr.fit(X_train, Y_train)
  return (
    mean_squared_error(lr.predict(X_train), Y_train),
    mean_squared_error(lr.predict(X_test), Y_test)
  )

splits = np.linspace(0.002, .998, 100)
train_error, test_error = zip(*[linear_regression_with_split(s) for s in splits])

fig, ax = plt.subplots(figsize=figsize)
ax.set_xlabel('Train Dataset [\\si{\\percent}]')
ax.plot(100*splits, train_error, label = 'MSE Train')
ax.plot(100*splits, test_error, label = 'MSE Test Error')
ax.legend()
fig.savefig('plot.pdf', format = 'pdf', bbox_inches = 'tight')

ax.set_ylim(0., 50.) # Zoomming
fig.savefig('plot-zoom.pdf', format = 'pdf', bbox_inches = 'tight')
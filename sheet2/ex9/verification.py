from sympy import symbols, simplify, pprint, expand
from sympy.printing.latex import latex
from scipy.special import binom

from analisi_dati import *


## Variance
n = symbols('n')

exp_val = n*(1-(1-1/n)**n)
exp_val2 = exp_val + n*(n-1)*(1+(1-2/n)**n-2*(1-1/n)**n)
var = exp_val2 - exp_val**2
var = simplify(expand(var))
pprint(exp_val, use_unicode = True)
pprint(exp_val2, use_unicode = True)
pprint(var, use_unicode = True)

# print(latex(var))

## Distributuon
def a(k, n):
  n_float = np.float64(n)
  k_float = np.float64(k)
  res = np.float64(0)
  for i in np.arange(0,k, dtype=np.float64):
    res += (-1)**i * binom(k_float,i) * np.power(k_float-i, n_float)
  return res

def p(k,n):
  n_float = np.float64(n)
  k_float = np.float64(k)
  return np.power(n_float,-n_float) * int(binom(n_float,k_float)) * a(k_float,n_float)

nv = np.float64(50)
values = np.arange(1,nv+1, dtype=np.float64)
distribution = np.array([p(k,nv) for k in values])
print('Normalization: ', np.sum(distribution))
print('Expected Value: ', np.sum(values*distribution))
print('Variance: ', np.sum(values*values*distribution)-np.power(np.sum(values*distribution), 2.))
print('Expected value (from formula): ', float(exp_val.subs(n, nv)))
print('Variance (from formula): ', float(var.subs(n, nv)))

values10 = np.arange(1, 11, dtype=np.float64)
distribution10 = np.array([p(k,nv) for k in values10])

fig, ax = plt.subplots(figsize=figsize)
ax.bar(values, distribution, label = '$n=50$')
ax.set_xlabel('$D$')
ax.set_ylabel('$\\mathbb{P}{\\left[D \\middle| n=50\\right]}$')
fig.savefig('distribution.pdf', format = 'pdf', bbox_inches = 'tight')





\begin{exercise}{2.9}
  \paragraph{Part A} Let \(D_i\) the random variable that counts the number of occurrences of the \(i\)th element in a bootstrap sample. Then we can write \(D\) as
  \[
    D = \sum_{i=1}^{n} \IndicatorCond{D_i>0}.
  \] 
  We can use this expression to compute the expected value of \(D\):
  \begin{align*}
    \ExpVal{D} &= \sum_{i=1}^{n} \ExpVal{\IndicatorCond{D_i>0}}
                = n \ExpVal{\IndicatorCond{D_1>0}}
                = n \Prob{D_1 \neq 0} = n \left(1-\Prob{D_1 = 0}\right) \\
               &= n \left(1-\left(\frac{n-1}{n}\right)^n \right)
                = n \left(1- \left(1-\frac1n\right)^n\right),
  \end{align*}
  where we used the fact that the \(D_i\)s variables are all identically distributed.
  It's interesting to note that for \(n\to+\infty\) the expected value is asymptotically \(n\left(1-\frac1e\right)\).
  
  The variance can be calculated using
  \[
    \Variance{D} = \ExpVal{D^2} - \left(\ExpVal{D}\right)^2.
  \]
  Calculating the first term
  \begin{align*}
    \ExpVal{D^2} &= \ExpVal{\left(\sum_{i=1}^{n} \IndicatorCond{D_i>0}\right)}
                  = \ExpVal{\sum_{i=1}^{n} \IndicatorCond{D_i>0}^2 + \sum_{i,j=1, i\neq j}^{n} \IndicatorCond{D_i>0}\IndicatorCond{D_j>0}} \\
                 &= \ExpVal{\sum_{i=1}^{n} \IndicatorCond{D_i>0} + \sum_{i,j=1, i\neq j}^{n} \IndicatorCond{D_i>0}\IndicatorCond{D_j>0}} 
                  = \sum_{i=1}^{n} \ExpVal{\IndicatorCond{D_i>0}} + \sum_{i,j=1, i\neq j}^{n} \ExpVal{\IndicatorCond{D_i>0}\IndicatorCond{D_j>0}} \\
                 &= \ExpVal{D} + n(n-1) \ExpVal{\IndicatorCond{D_1>0}\IndicatorCond{D_2>0}}
                  = \ExpVal{D} + n(n-1) \Prob{D_1 \neq 0, D_2 \neq 0 } \\
                 &= \ExpVal{D} + n(n-1) \left(1 - \Prob{D_1=0 \vee D_2=0 } \right) \\
                 &= \ExpVal{D} + n(n-1) \left(1 - \Prob{D_1=0} -\Prob{D_2=0} + \Prob{D_1=0,D_2=0}\right) \\
                 &= \ExpVal{D} + n(n-1) \left(1 - 2\left(\frac{n-1}{n}\right)^n + \left(\frac{n - 2}{n}\right)^{n}\right)
  \end{align*}
  Putting it back in the expression of the variance we get:
  \[
    \Variance{D} = n \left(n \left(\frac{n - 2}{n}\right)^{n} - n \left(\frac{n - 1}{n}\right)^{2 n} - \left(\frac{n - 2}{n}\right)^{n} + \left(\frac{n - 1}{n}\right)^{n}\right).
  \]
  
  \paragraph{Part B} Let's compute the full distribution now. The problem can be rephrased in calculating the probability that there are \(D\) distinct symbol in a string chosen uniformly among all the strings of length \(n\) composed with an alphabet of \(n\) symbols.
  
  Let \(a_{n,k}\) the number of strings of length \(n\) composed with an alphabet of \(k\) symbols, and where each symbol appears at least one time in the string.
  Using the \emph{inclusion-exclusion principle}\footnote{
    Actually the principle is applied to count the number of strings where not all symbols appear. Taking the complementary we get expression of \(a_{n,k}\).
  }
  can be shown that
  \[
    a_{n,k} = \sum_{i=0}^{k} (-1)^i \binom{k}{i} (k-i)^n.
  \]
  
  The number of all possible strings is \(n^n\). The probability of getting a string with exactly \(D\) different symbols is \(a_{n,D}\) multiplied by the number of ways of choosing \(D\) symbols form the alphabet of \(n\), and divided by number of all possible strings:
  \[
    \Prob{D} = \frac{a_{n,D}\binom{n}{D}}{n^n} =
                      \frac1{n^n}\binom{n}{D} \sum_{i=0}^{D} (-1)^i \binom{D}{i} (D-i)^n
  \]
  Figure~\ref{fig:2.9} shows the distribution for \(n=50\).
  
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{\RootPathSheetTwo/ex9/distribution.pdf}
    \caption{probability mass function of \(D\) with \(n=50\). The expected value is \(31.7915\), the variance \(4.8799\).}
    \label{fig:2.9}
  \end{figure}
  
  \pythoncode{\RootPathSheetTwo/ex9/verification.py}
\end{exercise}
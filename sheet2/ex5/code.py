from sklearn.datasets import make_regression
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

from analisi_dati import *

n_samples = 2000
max_fold = n_samples/10

X, Y, true_coef = make_regression(
  n_samples = n_samples,
  n_features = int(n_samples/10),
  n_informative = int(n_samples/50),
  noise = .1,
  random_state = 0,
  coef = True
)

def mse_stat(model, X_test, Y_test):
  predictedY = model.predict(X_test)
  squared_errors = (Y_test-predictedY)**2
  return np.mean(squared_errors), np.std(squared_errors)

## Theoretical
theo = LinearRegression()
theo.coef_ = true_coef
theo.intercept_ = 0.
print('Theoretical: ', mse_stat(theo, X, Y))

# Hold Out Set
from sklearn.model_selection import train_test_split

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size=0.8, random_state=0)
hos = LinearRegression()
hos.fit(X_train, Y_train)

print('Hold Out Set: ', mse_stat(hos, X_test, Y_test))


## LeaveOneOut
from sklearn.model_selection import LeaveOneOut

def error_given_indexes(train_i, test_i):
  lri = LinearRegression()
  lri.fit(X[train_i], Y[train_i])
  return mean_squared_error(lri.predict(X[test_i]), Y[test_i])

loo = LeaveOneOut()
loo_err = np.array([error_given_indexes(tr, ts) for tr, ts in pbar(loo.split(X), total = len(X))])

print(f'Leave One Out: ({np.mean(loo_err)}, {np.std(loo_err)})')

## k-fold CrossValidation
from sklearn.model_selection import KFold

def kfold(k, random_state = None):
  kfold = KFold(n_splits=k, shuffle=True, random_state=random_state)
  errs = np.array([error_given_indexes(tr, ts) for tr, ts in kfold.split(X)])
  return np.mean(errs), np.std(errs)

kfold_mean_err, kfold_std_err = kfold(5, random_state=1)

print(f'k-fold: ({kfold_mean_err}, {kfold_std_err})')

# k-fold plot
fold_range = np.arange(2, min(n_samples, max_fold)+1, dtype=np.uint32)
fold_err, fold_std = map(np.array, zip(*[kfold(k) for k in pbar(fold_range)]))


fig, ax = plt.subplots(figsize=figsize)
ax.set_xlabel('$k$')
ax.set_ylabel('$k$-fold crossvalidation')
# ax.errorbar(fold_range, fold_err, fold_std)
ax.plot(fold_range, fold_err)
fig.savefig('kfold.pdf', format = 'pdf', bbox_inches = 'tight')

ax.set_xlim((1.5, 15.5))
fig.savefig('kfold-zoom.pdf', format = 'pdf', bbox_inches = 'tight')






import numpy as np
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import Sequential
from tensorflow.keras.layers import  Input, Dense
from sklearn.model_selection import train_test_split

from analisi_dati import *

batch_size = 10
epochs = 20

X, Y = mnist_dataset()

X_train, X_test, Y_train, Y_test = train_test_split(
  X, Y,
  train_size=0.85,
  random_state=random_state
)

X_train /= 255.
X_test  /= 255.
Y_train = to_categorical(Y_train, 10)
Y_test = to_categorical(Y_test, 10)

model = Sequential(
    [
        Input(shape=X_train.shape[1]),
        Dense(35, activation='sigmoid'),
        Dense(Y_train.shape[1], activation='softmax'),
    ]
)
model.summary()

model.compile(loss='categorical_crossentropy', optimizer='SGD', metrics=['accuracy'])
model.fit(X_train, Y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)

score = model.evaluate(X_test, Y_test, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])
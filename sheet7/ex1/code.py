from matplotlib.pyplot import axis
from sklearn.linear_model import SGDRegressor
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_regression
from sklearn.utils import gen_batches, shuffle

from analisi_dati import *

import time
import warnings
warnings.filterwarnings("ignore")

# tolerance = 1e-4
# n_stability = 100
# n_consecutive_to_stop = 50
# min_n_epoch = n_stability
# learning_rate_initial_value = 0.000001

tolerance = 1e-4
n_stability = 20
n_consecutive_to_stop = 10
min_n_epoch = n_stability
learning_rate_initial_value = 0.000001


X, Y = boston_dataset()

X_train, X_test, Y_train, Y_test = train_test_split(
  X, Y,
  train_size=400,
  random_state=random_state
)

class MiniBatchSGDRegressor():
  """
  The (inverse) loss function is R2, the score used in linear regression also in
  sklearn. Essatialy it's the same as the quared loss function.
  """
  def __init__(self, learning_rate, batch_size, X_dim):
    self._learning_rate = learning_rate
    self._bacth_size = batch_size
    self._w = np.random.rand(X_dim)
    self._b = np.random.rand()
    print(self._w)

  def _wx_b_y(self, X, Y):
    return np.sum(self._w*X, axis=1) + self._b - Y
  
  def partial_fit(self, X, Y):
    wx_b_y = self._wx_b_y(X, Y)
    y_var = np.var(Y)
    if y_var == 0.:
      y_var = 1.
    w_gradient = -2*X.T*wx_b_y/y_var
    b_gradient = -2*wx_b_y/y_var

    w_grad_average = np.mean(w_gradient, axis=1)
    b_grad_average = np.mean(b_gradient, axis=0)

    self._w += w_grad_average * self._learning_rate
    self._b += b_grad_average * self._learning_rate

  def score(self, X, Y):
    return 1-np.mean((self._wx_b_y(X, Y))**2)/np.var(Y)
    

  


def r2_and_iterations(batch_size, verbose = True):
  print(batch_size)
  start_time = time.time()

  # In internet I've found a lot of people saying that using SGDRegressor with
  # partial_fit() method is a valid implementation of a minibatch SGD. 
  # For my understanding of documentation, this is not true. A guy on StackOverflow is 
  # claiming that partial_fit() runs a SGD with minibatch size =1, so it's not different
  # from using fit(); I agree with him.
  # I think partial fit it would be useful when dataset is so large that it can be 
  # contained in memory, but obviouslly it's not our case.
  #
  # sgdlr = SGDRegressor(
  #   eta0=learning_rate_initial_value/batch_size,
  #   learning_rate='constant',
  #   random_state=batch_size
  # )

  sgdlr = MiniBatchSGDRegressor(
    learning_rate=learning_rate_initial_value*batch_size,
    batch_size=batch_size,
    X_dim=X.shape[1]
  )

  n_epoch = 0
  last_n_r2_test = [-np.inf for _ in range(n_stability)]
  last_mean_r2 = -np.inf
  last_r2 = -np.inf
  n_consecutive = 0

  while True:
    # Stopping critirias
    if (
      (last_r2 - last_mean_r2)/abs(last_mean_r2) < tolerance and
      last_mean_r2 != -np.inf and
      n_epoch > min_n_epoch):
      n_consecutive += 1
    else:
      n_consecutive = 0
    if n_consecutive == n_consecutive_to_stop:
      break
    n_epoch += 1 

    # Batches splitting
    X_shuf, Y_shuf = shuffle(X_train,Y_train, random_state=n_epoch)
    batch_slices = list(gen_batches(X_shuf.shape[0], batch_size, min_batch_size=1))
    for sl in batch_slices:
      sgdlr.partial_fit(X_shuf[sl], Y_shuf[sl])

    # Computing metric averages
    last_n_r2_test.append(last_r2)
    del last_n_r2_test[0]
    last_mean_r2 = sum(last_n_r2_test)/n_stability
    last_r2 = sgdlr.score(X_test, Y_test)

    # Logging
    if (n_epoch % 1 == 0 and verbose):
      print(f'Batch size: {batch_size}, Epoch {n_epoch}, R2 = {last_r2}, mean R2 = {last_mean_r2}')
  
  enlapsed_time = time.time()-start_time
  return (sgdlr.score(X_test, Y_test), n_epoch, enlapsed_time)

bs = np.array([1,2,4,5,8,10,16,20,25,40,50,80,100,200,400])
scores, epochs, times = zip(*[r2_and_iterations(bss, False) for bss in bs])

fig, ax = plt.subplots(figsize=figsize)
ax.plot(bs, scores)
ax.set_xlabel('Batch Size')
ax.set_ylabel('$R^2$')
fig.savefig(f'bs-scores.pdf', format = 'pdf', bbox_inches = 'tight')

fig, ax = plt.subplots(figsize=figsize)
ax.plot(bs, epochs)
ax.set_xlabel('Batch Size')
ax.set_ylabel('Number of epochs')
fig.savefig(f'bs-epochs.pdf', format = 'pdf', bbox_inches = 'tight')

fig, ax = plt.subplots(figsize=figsize)
ax.plot(bs, times)
ax.set_xlabel('Batch Size')
ax.set_ylabel('Computing time')
ax.set_yscale('log')
fig.savefig(f'bs-times.pdf', format = 'pdf', bbox_inches = 'tight')


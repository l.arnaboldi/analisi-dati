from sklearn.datasets import make_classification
from sklearn.svm import SVC
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import uniform, loguniform
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split

from analisi_dati import *

cache_size = 3000

kernel_types = {
  'Linear':{'kernel':'linear'},
  'Poly. 2':{'kernel':'poly', 'degree':2},
  'Poly. 3':{'kernel':'poly', 'degree':3},
  'Poly. 4':{'kernel':'poly', 'degree':4},
  'RBF':{'kernel':'rbf'},
  'Sigmoid':{'kernel':'sigmoid'}
}

X, Y = make_classification(
  n_samples=100,
  n_features=20,
  n_informative=3,
  n_redundant=17,
  n_classes=2,
  class_sep=4,
  random_state=random_state,
  flip_y=0.07
)

X_train, X_test, Y_train, Y_test = train_test_split(
  X, Y,
  test_size=0.2,
  random_state=random_state
)

errors = []
svns = []

for kernel_name, kernel_params in kernel_types.items():
  print(f'Computing {kernel_name}...')
  model_optimizer = RandomizedSearchCV(
    SVC(**kernel_params, cache_size=cache_size),
    {'C': loguniform(1, 1000)},
    n_iter= 300
  )
  model_optimizer.fit(X_train, Y_train)

  best_params = model_optimizer.best_params_
  train_error = model_optimizer.best_score_
  test_error = model_optimizer.best_estimator_.score(X_test, Y_test)
  svn = model_optimizer.best_estimator_.support_vectors_.shape[0]
  print(f'  Best parameters: {best_params}')
  print(f'  Train: {train_error}')
  print(f'  Test: {test_error}')
  print(f'  SV number: {svn}')

  errors.append((train_error, test_error))
  svns.append(svn)

train_err, test_err = zip(*errors)
train_err = np.array(train_err)
test_err = np.array(test_err)
svns = np.array(svns)

fig, ax = plt.subplots(figsize=figsize)
bar_width = 0.35
bar_index = np.arange(len(train_err))
train_bar = ax.bar(bar_index - bar_width/2, 100*train_err, bar_width, label='Train Error')
test_bar = ax.bar(bar_index + bar_width/2, 100*test_err, bar_width, label='Test Error')
ax.legend()
ax.set_ylim(45,103)
ax.set_xticks(bar_index, kernel_types.keys())
ax.set_xlabel('Kernel')
ax.set_ylabel('Accuracy [$\\si{\\percent}$]')
fig.savefig('errors.pdf', format = 'pdf', bbox_inches = 'tight')

fig, ax = plt.subplots(figsize=figsize)
ax.bar(
  kernel_types.keys(),
  svns,
  color=sns.color_palette("pastel")[:len(svns)]
)
ax.set_xlabel('Kernel')
ax.set_ylabel('Support Vector Number')
fig.savefig('svns.pdf', format = 'pdf', bbox_inches = 'tight')
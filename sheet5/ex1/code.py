from sklearn.svm import SVC

from analisi_dati import *

C_min = -3
C_max = 2.2
cache_size = 3000

kernel_types = {
  'Linear':{'kernel':'linear'},
  'Polynomial deg. 2':{'kernel':'poly', 'degree':2},
  'Polynomial deg. 3':{'kernel':'poly', 'degree':3},
  'Polynomial deg. 4':{'kernel':'poly', 'degree':4},
  'Radial Basis Function':{'kernel':'rbf'},
  'Sigmoid':{'kernel':'sigmoid'}
}

X , Y = binary_mnist_dataset()
C_range = np.logspace(C_min, C_max, 100)

@np.vectorize
def svn_given_params(C, **kwargs):
  svm = SVC(C=C, cache_size=cache_size, **kwargs)
  svm.fit(X, Y)
  return svm.support_vectors_.shape[0]

fig, ax = plt.subplots(figsize=figsize)

for kernel_name, kernel_params in kernel_types.items():
  print(f'Computing {kernel_name}...', end='', flush=True)
  svn = np.array([svn_given_params(C, **kernel_params) for C in pbar(C_range)])
  ax.plot(C_range, svn, label=kernel_name)
  print(' Done!')
ax.set_xscale('log')
ax.set_xlabel('Regularization parameter $C$')
ax.set_ylabel('Number of Support Vectors')
ax.legend()
ax.set_xlim(1e-3, 1e2)
fig.savefig('svn.pdf', format = 'pdf', bbox_inches = 'tight')
ax.set_yscale('log')
fig.savefig('svn-log.pdf', format = 'pdf', bbox_inches = 'tight')
ax.set_yscale('linear')

ax.set_xlim(1e-2, 1e1)
ax.set_ylim(0.5, 3500)
fig.savefig('svn-zoom.pdf', format = 'pdf', bbox_inches = 'tight')
ax.set_yscale('log')
fig.savefig('svn-zoom-log.pdf', format = 'pdf', bbox_inches = 'tight')

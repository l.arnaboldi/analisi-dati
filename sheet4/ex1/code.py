from matplotlib.pyplot import xlim
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.utils import shuffle
from sklearn.base import ClassifierMixin

from analisi_dati import *

class ModelWithAccuracy():
  # Achtung!! It's important that the method is called score, since it is used 
  # in function cross_validate_score
  def score(self, X, Y):
    predictions = self.predict(X)
    
    correct_count = 0
    for y_pred, y_true in zip(predictions, list(Y)):
      if y_pred == y_true:
        correct_count += 1
    
    return correct_count/len(Y)

class KMeansClassifier(ModelWithAccuracy, KMeans):
  def fit(self, X, Y, **kwargs):
    KMeans.fit(self, X, Y, **kwargs)
    classes = set(Y)
    cluster_class_count = {cluster:{clas:0 for clas in classes} for cluster in self.labels_}

    for index, cluster in enumerate(self.labels_):
      cluster_class_count[cluster][Y[index]] += 1
    
    self.class_of_cluster_ = {}
    for cluster in self.labels_:
      self.class_of_cluster_[cluster] = max(
        cluster_class_count[cluster],
        key=cluster_class_count[cluster].get
      )

  def predict(self, X, **kwargs):
    cluster_prediction = KMeans.predict(self, X)
    class_prediction = list(map(
      lambda c: self.class_of_cluster_[c],
      cluster_prediction
    ))

    return np.array(class_prediction)

class CombinedKmeansClassifiers(ModelWithAccuracy, ClassifierMixin):
  def __init__(self, classes = None, **kwargs):
    if classes is None:
      self.classes_ = np.array([])
    else:
      self.classes_ = classes
    self.kmeans_args = kwargs
    self.classifiers_ = [KMeans(**kwargs) for _ in range(self.classes_.shape[0])]

  def fit(self, X, Y):
    for i, cl in enumerate(self.classes_):
      to_keep = (Y == cl)
      self.classifiers_[i].fit(X[to_keep], Y[to_keep])
    

  def predict(self, X):
    predictions = np.full((X.shape[0],), 0, dtype=self.classes_.dtype)
    actual_lower_distance = np.full((X.shape[0],), np.inf, dtype='float64')

    for ck, cl in zip(self.classifiers_, self.classes_):
      actual_distance = ck.transform(X).min(axis=1)
      assert(actual_distance.shape == actual_lower_distance.shape)
      for i, d in enumerate(actual_distance):
        if d < actual_lower_distance[i]:
          predictions[i] = cl
          actual_lower_distance[i] = d

    return predictions

  def get_params(self, deep=True):
    return {**self.kmeans_args, 'classes':self.classes_}   


X, Y = iris_dataset()
X_train, X_test, Y_train, Y_test = train_test_split(
  X, Y,
  test_size=0.2,
)

k_range = np.arange(2,int(4*X.shape[0]/5))

def fit_and_accuracy(model):
  model.fit(X_train, Y_train)
  return model.score(X_test, Y_test)

# Whole
whole_accuracy = np.array([
  np.mean(cross_val_score(KMeansClassifier(k), X, Y))
  # fit_and_accuracy(KMeansClassifier(k))
  for k in pbar(k_range)
])

# Partial
partial_accuracy = np.array([
  np.mean(cross_val_score(CombinedKmeansClassifiers(
    classes = np.array(list(set(Y))),
    n_clusters=int((k+2)/3)
  ), X, Y))
  # fit_and_accuracy(CombinedClassifier(...))
  for k in pbar(k_range)
])

fig, ax = plt.subplots(figsize=figsize)
ax.plot(k_range, 100*whole_accuracy, label = 'Whole dataset clustering')
ax.plot(k_range, 100*partial_accuracy, label = 'Classes clustering')
ax.set_xlabel('Number of clusters')
ax.set_ylabel('Accuracy $\\left[\\si{\\percent}\\right]$')
ax.legend()

ax.set_xlim(min(k_range), max(k_range))
fig.savefig('accuracies.pdf', format = 'pdf', bbox_inches = 'tight')

ax.set_xlim(min(k_range), 40)
fig.savefig('accuracies-zoom.pdf', format = 'pdf', bbox_inches = 'tight')
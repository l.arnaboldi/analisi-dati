from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
import warnings

from analisi_dati import *

models = {
  'KNN': (KNeighborsClassifier(), {'n_neighbors':list(range(1,16))}),
  'LDA': (LinearDiscriminantAnalysis(), {}),
  'QDA': (QuadraticDiscriminantAnalysis(), {}),
  'NB':  (GaussianNB(), {}),
  'LR':  (LogisticRegression(random_state=0), {})
}

print('Loading dataset...', end='',flush=True)
X, Y = mnist_dataset()
print(' Done!')

warnings.filterwarnings("ignore")

X_train, X_test, Y_train, Y_test = train_test_split(
  X, Y,
  test_size=10000,
  random_state=0
)

accuracies_test = []
accuracies_train = []

for name, model_data in models.items():
  model_optimizer = GridSearchCV(model_data[0], model_data[1])
  model_optimizer.fit(X_train, Y_train)
  model = model_optimizer.best_estimator_
  accuracy_test = model.score(X_test, Y_test)
  accuracy_train = model.score(X_train, Y_train)
  accuracies_test.append(accuracy_test)
  accuracies_train.append(accuracy_train)
  print(f'{name} accuracy: {accuracy_train} {accuracy_test}; params: {model_optimizer.best_params_}')

accuracies_test = np.array(accuracies_test)
accuracies_train = np.array(accuracies_train)

fig, ax = plt.subplots(figsize=figsize)
ax.bar(
  list(map(str.upper, models.keys())),
  100*accuracies_test,
  color=sns.color_palette("pastel")[:len(models)]
)
ax.set_xlabel('Training Algorithm')
ax.set_ylabel('Test Accuracy $\\left[\\si{\\percent}\\right]$')
fig.savefig('accuracies.pdf', format = 'pdf', bbox_inches = 'tight')

fig, ax = plt.subplots(figsize=figsize)
ax.bar(
  list(map(str.upper, models.keys())),
  100*accuracies_train,
  color=sns.color_palette("pastel")[:len(models)]
)
ax.set_xlabel('Training Algorithm')
ax.set_ylabel('Train Accuracy $\\left[\\si{\\percent}\\right]$')
fig.savefig('accuracies-train.pdf', format = 'pdf', bbox_inches = 'tight')